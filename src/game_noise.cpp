#include "game_noise.h"

static uint8 perlin_indices[512];

uint64 xor_x;
static uint64 xorshift_64()
{
	xor_x ^= xor_x >> 12;
	xor_x ^= xor_x << 25;
	xor_x ^= xor_x >> 27;
	return xor_x * 2685821657736338717ul;
}

void seed_perlin(uint64 seed)
{
	xor_x = (seed + 1) * 12907;
	for (int i = 0; i < 256; ++i)
	{
		perlin_indices[i] = (uint8)i;
		perlin_indices[i + 256] = (uint8)i;
	}
	for (int i = 0; i < 512; i++)
	{
		int r = xorshift_64() % 512;
		uint8 swap = perlin_indices[i];
		perlin_indices[i] = perlin_indices[r];
		perlin_indices[r] = swap;
	}
}

static double plerp(double a, double b, double x)
{
	return a + (b - a) * x;
}

static double pgrad(int hash, double x, double y, double z)
{
	int h = hash & 15;
	double u = h < 8 ? x : y;
	double v;
	if (h < 4)
		v = y;
	else if (h ==12 || h == 14)
		v = x;
	else
		v = z;

	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

double perlin_noise(double x, double y, double z)
{
	// look-ups for cube bounds
	int xi = (int)x & 255;
	int yi = (int)y & 255;
	int zi = (int)z & 255;
	
	// Fractional values for fade
	double xf = x - (int)x;
	double yf = y - (int)y;
	double zf = z - (int)z;

	#define PERL_FADE(f) ((f) * (f) * (f) * ((f) * ((f) * 6 - 15) + 10))

	double u = PERL_FADE(xf);
	double v = PERL_FADE(yf);
	double w = PERL_FADE(zf);

	uint8 *p = &perlin_indices[0];

	int a = p[xi] + yi;
	int aa = p[a] + zi;
	int ab = p[a + 1] + zi;
	int b = p[xi + 1] + yi;
	int ba = p[b] + zi;
	int bb = p[b + 1] + zi;

	double x1, x2, y1, y2;
	x1 = lerp(pgrad(p[aa], xf, yf, zf), pgrad(p[ba], xf - 1, yf, zf), u);
	x2 = lerp(pgrad(p[ab], xf, yf - 1, zf), pgrad(p[bb], xf - 1, yf - 1, zf), u);
	y1 = lerp(x1, x2, v);

	x1 = lerp(pgrad(p[aa + 1], xf, yf, zf - 1), pgrad(p[ba + 1], xf - 1, yf, zf - 1), u);
	x2 = lerp(pgrad(p[ab + 1], xf, yf - 1, zf - 1), pgrad(p[bb + 1], xf - 1, yf - 1, zf - 1), u);	
	y2 = lerp(x1, x2, v);

	return (lerp(y1, y2, w) + 1) / 2;
}
