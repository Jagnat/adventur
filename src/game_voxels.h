#pragma once
#ifndef _GAME_VOXELS_H_
#define _GAME_VOXELS_H_

#define CHUNK_SIZE 64

struct Chunk
{
	uint64 seed;
	uint16 location[3];
	uint8 blocks[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
};

void gen_chunk(Chunk *chunk, uint16 x, uint16 y, uint16 z);
uint8 chunk_get(Chunk *chunk, int x, int y, int z);
void chunk_set(Chunk *chunk, int x, int y, int z, uint8 val);
void build_voxel_mesh(Chunk *chunk, ChunkMesh *mesh);
void build_dualcontouring_mesh(Chunk *chunk, ChunkMesh *mesh);

#endif
