
GLuint create_shader(char *shader_code, GLenum shader_type);
GLuint create_program(char *vertex_path, char *fragment_path);
char *load_code(char *filepath);

const float framebuffer_verts[8] = {-1, -1,
									1, -1,
									-1, 1,
									1, 1};

// TODO: Allocate elsewhere...?
RenderState *renderstate;

PlayerCam camera = {0};

uint *chunk_indices;

void render_init()
{
	log_info("Initializing renderer!");
	renderstate = (RenderState*)malloc(sizeof(RenderState));
	if (!renderstate)
		log_exit("failed to allocate renderstate!");
	renderstate->width = 1280;
	renderstate->height = 720;

	chunk_indices = (uint*)malloc(sizeof(uint) * 37748736);
	if (!chunk_indices)
		log_exit("Failed to allocate chunk indices!");

	for (int i = 0; i < CHUNK_INDICES_LENGTH / 6; ++i)
	{
		chunk_indices[i * 6 + 0] = i * 4 + 0;
		chunk_indices[i * 6 + 1] = i * 4 + 2;
		chunk_indices[i * 6 + 2] = i * 4 + 1;
		chunk_indices[i * 6 + 3] = i * 4 + 0;
		chunk_indices[i * 6 + 4] = i * 4 + 3;
		chunk_indices[i * 6 + 5] = i * 4 + 2;
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LEQUAL);

	renderstate->program_ids[PROGRAM_VOXEL] = create_program("shaders/pre_vert.glsl", "shaders/pre_frag.glsl");

	GLint proj_loc, model_loc, view_loc, cam_pos_loc;
	proj_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_VOXEL], "projection_matrix");
	if(proj_loc == -1)
		log_error("Failed to retrieve projection matrix shader location!");
	model_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_VOXEL], "model_matrix");
	if (model_loc == -1)
		log_error("Failed to retrieve model matrix shader location!");
	view_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_VOXEL], "view_matrix");
	if (view_loc == -1)
		log_error("Failed to retrieve view matrix shader location!");
	cam_pos_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_VOXEL], "cam_pos");
	if (cam_pos_loc == -1)
		log_error("Failed to retrieve camera pos shader location!");

	renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_PROJ] = proj_loc;
	renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_MODEL] = model_loc;
	renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_VIEW] = view_loc;
	renderstate->uniform_locs[PROGRAM_VOXEL][LOC_CAM_POS] = cam_pos_loc;

	glGenTextures(1, &renderstate->framebuffer_texture);
	glBindTexture(GL_TEXTURE_2D, renderstate->framebuffer_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, renderstate->width, renderstate->height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenRenderbuffers(1, &renderstate->renderbuffer_id);
	glBindRenderbuffer(GL_RENDERBUFFER, renderstate->renderbuffer_id);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
		renderstate->width, renderstate->height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &renderstate->framebuffer_id);
	glBindFramebuffer(GL_FRAMEBUFFER, renderstate->framebuffer_id);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, renderstate->framebuffer_texture, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER, renderstate->renderbuffer_id);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		log_error("Failed to complete framebuffer!");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenBuffers(1, &renderstate->frame_vertexbuffer_id);
	glBindBuffer(GL_ARRAY_BUFFER, renderstate->frame_vertexbuffer_id);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float),
		framebuffer_verts, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	renderstate->program_ids[PROGRAM_POST] = create_program("shaders/post_vert.glsl", "shaders/post_frag.glsl");

	GLint fbo_tex_loc, reso_loc;
	fbo_tex_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_POST], "fbo_texture");
	if (fbo_tex_loc == -1)
		log_error("failed to get fbo uniform location!");
	reso_loc = glGetUniformLocation(renderstate->program_ids[PROGRAM_POST], "resolution");
	if (reso_loc == -1)
		log_error("Failed to get resolution uniform location!");
	renderstate->uniform_locs[PROGRAM_POST][LOC_FBO_TEXTURE] = fbo_tex_loc;
	renderstate->uniform_locs[PROGRAM_POST][LOC_RESOLUTION] = reso_loc;

	log_info("Finished render initialization!");
}

void set_cam(PlayerCam cam)
{
	camera = cam;
	renderstate->matrix_view = Mat4_FPSCam(cam.pos, cam.yaw, cam.pitch);
}

void chunkmesh_buffer_init(ChunkMesh *mesh)
{
	glGenVertexArrays(1, &mesh->ids[0]);
	glGenBuffers(2, &mesh->ids[1]);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->ids[1]);
	glBufferData(GL_ARRAY_BUFFER, mesh->num_vertices * 12, mesh->vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ids[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->num_indices * sizeof(uint), chunk_indices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(mesh->ids[0]);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->ids[1]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ids[2]);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	const float stride = 12;
	glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, stride, 0);
	glVertexAttribIPointer(1, 1, GL_BYTE, stride, (void*)(3));
	glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, stride, (void*)(3 + 1));
	glVertexAttribPointer(3, 2, GL_UNSIGNED_BYTE, GL_FALSE, stride, (void*)(3 + 1 + 4));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void render_start()
{
	glBindFramebuffer(GL_FRAMEBUFFER, renderstate->framebuffer_id);
	glUseProgram(renderstate->program_ids[PROGRAM_VOXEL]);
	glClearColor(0.1f, 0.1f, 0.1f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void render_voxels(ChunkMesh *mesh)
{
	// Render world to framebuffer
	glUniformMatrix4fv(renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_VIEW],
		1, false, renderstate->matrix_view.flat);
	glUniformMatrix4fv(renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_PROJ],
		1, false, renderstate->matrix_proj.flat);
	glUniformMatrix4fv(renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_MODEL],
		1, false, mesh->model_matrix.flat);

	glBindVertexArray(mesh->ids[0]);
	glDrawElements(GL_TRIANGLES, mesh->num_indices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	// Push back identity model matrix for future use
	glUniformMatrix4fv(renderstate->uniform_locs[PROGRAM_VOXEL][LOC_MATRIX_MODEL],
		1, false, Mat4_Identity().flat);
}

void render_flush()
{
	// Render framebuffer to screen
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(renderstate->program_ids[PROGRAM_POST]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, renderstate->framebuffer_texture);
	glUniform1i(renderstate->uniform_locs[PROGRAM_POST][LOC_FBO_TEXTURE], 0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, renderstate->frame_vertexbuffer_id);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDisableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_resize(uint width, uint height)
{
	renderstate->width = width;
	renderstate->height = height;
	log_info("Renderer Resizing to %dx%d", width, height);
	glViewport(0, 0, width, height);
	renderstate->matrix_proj = Mat4_PerspectiveFOV((70.f * 3.14159f) / 180.f,
		(float)width / (float)height, 0.01f, 100000.f);

	glBindTexture(GL_TEXTURE_2D, renderstate->framebuffer_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, renderstate->renderbuffer_id);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
		width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glUseProgram(renderstate->program_ids[PROGRAM_POST]);
	//glUniform2f(post_reso_loc, viewport_w, viewport_h);
}

GLuint create_program(char *vertex_path, char *fragment_path)
{
	char *vertex_code = load_code(vertex_path);
	if (!vertex_code)
	{
		log_error("Failed to load vertex code: %s", vertex_path);
		return 0;
	}
	char *fragment_code = load_code(fragment_path);
	if (!fragment_code)
	{
		log_error("Failed to load fragment code: %s", fragment_path);
		free(vertex_code);
		return 0;
	}
	GLuint vertex_id = create_shader(vertex_code, GL_VERTEX_SHADER);
	GLuint fragment_id = create_shader(fragment_code, GL_FRAGMENT_SHADER);
	if (!vertex_id || !fragment_id)
	{
		log_error("Failed to load shader(s)! V:%d,F:%d", vertex_id, fragment_id);
		return 0;
	}
	GLuint prog_id = glCreateProgram();
	glAttachShader(prog_id, vertex_id);
	glAttachShader(prog_id, fragment_id);
	free(vertex_code);
	free(fragment_code);
	glLinkProgram(prog_id);
	int status = 0;
	glGetProgramiv(prog_id, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		int len = 0;
		glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &len);
		char *infolog = (char*)malloc(len);
		glGetProgramInfoLog(prog_id, len,  0, infolog);
		log_error("Failed to link shader program:\n%s", infolog);
		free(infolog);
		return 0;
	}
	return prog_id;
}

GLuint create_shader(char *shader_code, GLenum shader_type)
{
	GLuint id = 0;
	id = glCreateShader(shader_type);
	if (id == 0)
		return 0;
	glShaderSource(id, 1, &shader_code, 0);
	glCompileShader(id);

	int status = 0;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		int len = 0;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
		char *infolog = (char*)malloc(len);
		glGetShaderInfoLog(id, len, 0, infolog);
		log_error("Failed to compile shader:\n%s", infolog);
		free(infolog);
		glDeleteShader(id);
		return 0;
	}
	return id;
}

char *load_code(char *filepath)
{
	FILE *fp = fopen(filepath, "rb");
	if (!fp)
	{
		log_error("Failed to load shader file: %s\n", filepath);
		return 0;
	}
	fseek(fp, 0, SEEK_END);
	int size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char *text = (char*)malloc(size + 1);
	if (!text)
	{
		log_error("Failed to allocate memory!");
		return 0;
	}
	fread(text, 1, size, fp);
	text[size] = 0;
	return text;
}
