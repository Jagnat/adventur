#pragma once
#ifndef _GAME_NOISE_H_
#define _GAME_NOISE_H_

void seed_perlin(uint64 seed);
double perlin_noise(double x, double y, double z);

#endif
