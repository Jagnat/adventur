#ifndef _GAME_PLATFORM_H_
#define _GAME_PLATFORM_H_

#include <stdint.h>

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint;
typedef uint64_t uint64;

typedef int8_t int8;
typedef int16_t int16;
typedef int64_t int64;

struct ButtonState
{
	bool endedDown;
	uint8 numStateSwitches;
};

#define NUM_BUTTONS 8
struct InputState
{
	union
	{
		ButtonState buttons[NUM_BUTTONS];
		struct
		{
			ButtonState forwardBtn;
			ButtonState backwardBtn;
			ButtonState leftBtn;
			ButtonState rightBtn;
			ButtonState upBtn;
			ButtonState downBtn;
			ButtonState leftMouseBtn;
			ButtonState rightMouseBtn;
		};
	};

	int mouseX, mouseY;
	bool mouseLocked;
};

struct PlatformState
{
	double targetUpdateMs;
	double targetRenderMs;
	InputState input;
	int clientWidth, clientHeight;
	bool resized;
	double elapsedTime;
};

// Called by platform
void GameInit();
void GameUpdate(PlatformState *platform);
void GameRender(double renderInterval);

// Called by game
void PlatformSetMouseLocked(bool locked);

#endif
