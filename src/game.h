#pragma once
#ifndef _GAME_H_
#define _GAME_H_

#include "Primitives.h"
#include "game_render.h"

void init_engine();
double elapsed_ms();

float lerp(float a, float b, float x);
float loop_in_range(float min, float max, float val);
float clamp(float min, float max, float val);

void interpolate_camera(float interval);

struct GameState
{
	bool gameInitialized = false;
	bool gamePaused = true;
	ChunkMesh *mesh;
	PlayerCam camPrev, camCurrent;
};

#endif
