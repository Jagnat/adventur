#version 330 core

uniform vec2 resolution;
uniform float FXAA_SUBPIX_SHIFT = 0.0;

layout(location = 0) in vec2 in_Position;
out vec2 out_Texel;

out vec4 posPos;

void main(void)
{
	gl_Position = vec4(in_Position, 0, 1);
	out_Texel = (in_Position + 1.0) / 2.0;
	vec2 rcpFrame = vec2(1.0 / resolution.x, 1.0 / resolution.y);
	posPos.xy = out_Texel.xy;
	posPos.zw = out_Texel.xy - (rcpFrame * (0.5 + FXAA_SUBPIX_SHIFT));
}
