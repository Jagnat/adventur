#pragma once
#ifndef _GAME_RENDER_GL_H_
#define _GAME_RENDER_GL_H_

#include "Primitives.h"

void render_init();
void render_resize(uint width, uint height);

#define MAX_PROGRAMS 8
#define MAX_UNIFORMS 16

#define PROGRAM_POST 0
#define PROGRAM_VOXEL 1

// uniforms for PROGRAM_POST
#define LOC_FBO_TEXTURE 0
#define LOC_RESOLUTION 1

// uniforms for PROGRAM_VOXEL
#define LOC_MATRIX_MODEL 0
#define LOC_MATRIX_VIEW 1
#define LOC_MATRIX_PROJ 2
#define LOC_CAM_POS 3

struct PlayerCam
{
	// yaw: 0 - 2pi
	float yaw;
	// pitch: -pi/2 - pi/2
	float pitch;
	Vec3 pos;
};

struct RenderState
{
	GLuint program_ids[MAX_PROGRAMS];
	GLint uniform_locs[MAX_PROGRAMS][MAX_UNIFORMS];

	Mat4 matrix_proj;
	Mat4 matrix_view;

	uint width, height;

	GLuint framebuffer_id;
	GLuint renderbuffer_id;
	GLuint frame_vertexbuffer_id;
	GLuint framebuffer_texture;
};

#define CHUNK_INDICES_LENGTH (4718592)
extern uint *chunk_indices;

struct ChunkMesh
{
	Mat4 model_matrix;
	// NOTE: 0 is VAO, 1 is VBO, 2 is IBO
	GLuint ids[3];
	uint num_indices, num_vertices;
	//uint indices[4718592];
	uint8 vertices[37748736];
};

void chunkmesh_buffer_init(ChunkMesh *mesh);

void render_start();
void render_flush();
void render_voxels(ChunkMesh *mesh);
void set_cam(PlayerCam cam);

#endif
