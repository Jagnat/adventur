#ifndef _WIN32_ADVENTUR_H_
#define _WIN32_ADVENTUR_H_

#include <windows.h>

struct Win32Data
{
	WNDCLASSEX windowClass;
	HWND window;
	HDC deviceContext;
	HGLRC glRenderContext;
};

#endif
