// Copyright (C) 2014, Jagannath Natarajan.
#include "game.h"

#include <glew.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>

#include "primitives.h"
#include "log.h"
#include "game_render.h"
#include "game_voxels.h"
#include "game_noise.h"
#include "game_platform.h"

bool paused = true;

GameState _state;
GameState *state = &_state;

PlayerCam interpolate_camera(PlayerCam *c0, PlayerCam *c1, double interval)
{
	PlayerCam cam = {0};
	cam.pos = Vec3_Lerp(c0->pos, c1->pos, interval);

	// PITCH:
	cam.pitch = lerp(c0->pitch, c1->pitch, interval);

	// YAW:
	cam.yaw = lerp(c0->yaw, c1->yaw, interval);
	cam.yaw = loop_in_range(0, 2 * M_PI, cam.yaw);

	return cam;
}

void GameInit()
{
	render_init();
	render_resize(1280, 720);

	state->camCurrent.pos = {-15, 79, -15};
	state->camCurrent.yaw = (5 * M_PI ) / 4;
	state->camCurrent.pitch = -0.698398;
	state->camPrev = state->camCurrent;

	Chunk *global_chunk;
	global_chunk = (Chunk*)calloc(1, sizeof(Chunk));
	state->mesh = (ChunkMesh*)malloc(sizeof(ChunkMesh));
	gen_chunk(global_chunk, 0, 0, 0);
	
	build_voxel_mesh(global_chunk, state->mesh);
	chunkmesh_buffer_init(state->mesh);

	state->gameInitialized = true;
	log_info("Initialized game!");
}

void GameUpdate(PlatformState *platform)
{
	if (platform->input.rightMouseBtn.numStateSwitches > 0 &&
		!platform->input.rightMouseBtn.endedDown)
	{
		PlatformSetMouseLocked(!platform->input.mouseLocked);
		state->gamePaused = !platform->input.mouseLocked;
	}

	state->camPrev = state->camCurrent;

	if (!state->gamePaused)
	{
		// Handle rotation
		const float mouse_sensitivity = 0.01f;

		state->camCurrent.yaw = state->camCurrent.yaw -
		(platform->input.mouseX - platform->clientWidth / 2) * mouse_sensitivity;

		state->camCurrent.pitch = clamp(-M_PI / 2 + 0.01f, M_PI / 2 - 0.01f,
			state->camCurrent.pitch - (platform->input.mouseY - platform->clientHeight / 2)
			* mouse_sensitivity);

		Vec3 added_pos = {0};
		const float speed = 0.1f;
		// Handle position
		if (platform->input.forwardBtn.endedDown)
		{
			added_pos.x += speed * cos(state->camCurrent.yaw + M_PI / 2);
			added_pos.z -= speed * sin(state->camCurrent.yaw + M_PI / 2);
		}
		if (platform->input.backwardBtn.endedDown)
		{
			added_pos.x += speed * cos(state->camCurrent.yaw - M_PI / 2);
			added_pos.z -= speed * sin(state->camCurrent.yaw - M_PI / 2);
		}
		if (platform->input.leftBtn.endedDown)
		{
			added_pos.x += speed * cos(state->camCurrent.yaw - M_PI);
			added_pos.z -= speed * sin(state->camCurrent.yaw - M_PI);	
		}
		if (platform->input.rightBtn.endedDown)
		{
			added_pos.x += speed * cos(state->camCurrent.yaw);
			added_pos.z -= speed * sin(state->camCurrent.yaw);
		}
		if (platform->input.upBtn.endedDown)
			added_pos.y += speed;
		if (platform->input.downBtn.endedDown)
			added_pos.y -= speed;
		state->camCurrent.pos = state->camCurrent.pos + added_pos;
	}
	state->camCurrent.yaw = loop_in_range(0, 2 * M_PI, state->camCurrent.yaw);

	// state->mesh->model_matrix = 
	// 	Mat4_RotY(platform->elapsedTime / 7000.0) * Mat4_Translate(-32, 0, -32);
}

void GameRender(double renderInterval)
{
	if (state->gameInitialized)
	{
		interpolate_camera(&state->camPrev, &state->camCurrent, renderInterval);
		set_cam(state->camCurrent);
		render_start();
		render_voxels(state->mesh);
		render_flush();
	}
}


float lerp(float a, float b, float x)
{
	return (1 - x) * a + x * b;
}

float loop_in_range(float min, float max, float val)
{
	float real_val = val - min;
	float real_max = max - min;
	float mod = real_val - real_max * floor(real_val / real_max);
	return mod + min;
}

float clamp(float min, float max, float val)
{
	float r = val;
	if (r > max)
		r = max;
	if (r < min)
		r = min;
	return r;
}

void set_bit_32(int *var, int bit)
{
	*var |= (1 << bit);
}

void clear_bit_32(int *var, int bit)
{
	*var &= ~(1 << bit);
}

bool is_bit_set_32(int var, int bit)
{
	return var & (1 << bit);
}

// Source file includes
#include "primitives.cpp"
#include "game_render.cpp"
#include "log.cpp"
#include "game_voxels.cpp"
#include "game_noise.cpp"
