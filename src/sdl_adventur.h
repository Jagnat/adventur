#pragma once
#ifndef _SDL_ADVENTUR_H_
#define _SDL_ADVENTUR_H_
#include <SDL.h>

struct SdlData
{
	SDL_Window *window;
	SDL_GLContext context;
};

#endif
