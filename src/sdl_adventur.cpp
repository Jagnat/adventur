#include "sdl_adventur.h"

#include <SDL.h>

SdlData sdlData;

int main (int argc, char **argv)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
		return 1;

	sdlData.window = SDL_CreateWindow("Adventur",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		1280, 720,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	SDL_Delay(2000);
	return 0;
}