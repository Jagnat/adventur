
void gen_chunk(Chunk *chunk, uint16 xloc, uint16 yloc, uint16 zloc)
{
	chunk->seed = 493875762ul;
	seed_perlin(chunk->seed);

	chunk->location[0] = xloc;
	chunk->location[1] = yloc;
	chunk->location[2] = zloc;
	//srand(0);
	for (int x = 0; x < CHUNK_SIZE; ++x)
	{
		for (int y = 0; y < CHUNK_SIZE; ++y)
		{
			for (int z = 0; z < CHUNK_SIZE; ++z)
			{
				#define PERL_RES 15.l
				double perlin = perlin_noise(x / PERL_RES, 12.421, z / PERL_RES);
				if ((int)(perlin * CHUNK_SIZE) > y)
					chunk_set(chunk, x, y, z, 255);
				else
					chunk_set(chunk, x, y, z, 0);
			}
		}
	}
}

struct mb_Color
{
	uint8 r, g, b, a;
};

static void set_vert(uint8* index, uint8 x, uint8 y, uint8 z, int8 norm, mb_Color c)
{
	index[0] = x;
	index[1] = y;
	index[2] = z;
	((int8*)index)[3] = norm;
	*(mb_Color*)(&index[4]) = c;
}

void build_dualcontouring_mesh (Chunk *chunk, ChunkMesh *mesh)
{
	/* For a first attempt, I will pretend that all
	 * my hermite data uses 0.5 with vectors facing from solid to empty.
	*/
	mesh->num_vertices = 0;
	mesh->num_indices = 0;
	mesh->model_matrix = Mat4_Translate(chunk->location[0] * CHUNK_SIZE,
										chunk->location[1] * CHUNK_SIZE,
										chunk->location[2] * CHUNK_SIZE);
	
}

void build_voxel_mesh(Chunk *chunk, ChunkMesh *mesh)
{
	mesh->num_vertices = 0;
	mesh->num_indices = 0;
	mesh->model_matrix = Mat4_Translate(chunk->location[0] * CHUNK_SIZE,
										chunk->location[1] * CHUNK_SIZE,
										chunk->location[2] * CHUNK_SIZE);

	int index = 0;
	for (uint8 x = 0; x < CHUNK_SIZE; ++x)
	{
		for (uint8 y = 0; y < CHUNK_SIZE; ++y)
		{
			for (uint8 z = 0; z < CHUNK_SIZE; ++z)
			{
				if (chunk_get(chunk, x, y, z) == 255)
				{
					uint8 *mv = &mesh->vertices[0];
					int s = 12;
					// NOTE: Temporary size values.

					#define rand_range(min, max) ((rand() % ((max) - (min))) + (min))

					/*mb_Color top = {rand_range(30, 50),
									rand_range(170, 220),
									rand_range(30, 50), 255};
					mb_Color sides = {rand_range(100, 130),
									  rand_range(60, 90),
									  rand_range(50, 80), 255};*/
					mb_Color a = {rand_range(0, 30), rand_range(150, 255), rand_range(150, 255)};
					mb_Color r = a, g = a, b = a;
					//mb_Color r = {255, 0, 0, 255}, g = {0, 255, 0, 255}, b = {0, 0, 255, 255};

					if (chunk_get(chunk, x + 1, y, z) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 1, y + 0, z + 1, 0, r);
						set_vert(&mv[index * s * 4 + s * 1], x + 1, y + 1, z + 1, 0, r);
						set_vert(&mv[index * s * 4 + s * 2], x + 1, y + 1, z + 0, 0, r);
						set_vert(&mv[index * s * 4 + s * 3], x + 1, y + 0, z + 0, 0, r);
						index++;
					}
					if (chunk_get(chunk, x - 1, y, z) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 0, y + 0, z + 0, 1, r);
						set_vert(&mv[index * s * 4 + s * 1], x + 0, y + 1, z + 0, 1, r);
						set_vert(&mv[index * s * 4 + s * 2], x + 0, y + 1, z + 1, 1, r);
						set_vert(&mv[index * s * 4 + s * 3], x + 0, y + 0, z + 1, 1, r);
						index++;
					}
					if (chunk_get(chunk, x, y + 1, z) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 0, y + 1, z + 1, 2, g);
						set_vert(&mv[index * s * 4 + s * 1], x + 0, y + 1, z + 0, 2, g);
						set_vert(&mv[index * s * 4 + s * 2], x + 1, y + 1, z + 0, 2, g);
						set_vert(&mv[index * s * 4 + s * 3], x + 1, y + 1, z + 1, 2, g);
						index++;
					}
					if (chunk_get(chunk, x, y - 1, z) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 0, y + 0, z + 0, 3, g);
						set_vert(&mv[index * s * 4 + s * 1], x + 0, y + 0, z + 1, 3, g);
						set_vert(&mv[index * s * 4 + s * 2], x + 1, y + 0, z + 1, 3, g);
						set_vert(&mv[index * s * 4 + s * 3], x + 1, y + 0, z + 0, 3, g);
						index++;
					}
					if (chunk_get(chunk, x, y, z + 1) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 0, y + 0, z + 1, 4, b);
						set_vert(&mv[index * s * 4 + s * 1], x + 0, y + 1, z + 1, 4, b);
						set_vert(&mv[index * s * 4 + s * 2], x + 1, y + 1, z + 1, 4, b);
						set_vert(&mv[index * s * 4 + s * 3], x + 1, y + 0, z + 1, 4, b);
						index++;
					}
					if (chunk_get(chunk, x, y, z - 1) == 0)
					{
						set_vert(&mv[index * s * 4 + s * 0], x + 1, y + 0, z + 0, 5, b);
						set_vert(&mv[index * s * 4 + s * 1], x + 1, y + 1, z + 0, 5, b);
						set_vert(&mv[index * s * 4 + s * 2], x + 0, y + 1, z + 0, 5, b);
						set_vert(&mv[index * s * 4 + s * 3], x + 0, y + 0, z + 0, 5, b);
						index++;
					}
				}
			}
		}
	}

	mesh->num_indices = index * 6;
	mesh->num_vertices = index * 4;
	log_info("num_vertices: %u, num_indices: %u", mesh->num_vertices, mesh->num_indices);

	// To be separated
}

uint8 chunk_get(Chunk *chunk, int x, int y, int z)
{
	if (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 || z >= CHUNK_SIZE)
		return 0;
	return chunk->blocks[x + y * CHUNK_SIZE + z * CHUNK_SIZE * CHUNK_SIZE];
}

void chunk_set(Chunk *chunk, int x, int y, int z, uint8 val)
{
	if (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 || z >= CHUNK_SIZE)
		return;
	chunk->blocks[x + y * CHUNK_SIZE + z * CHUNK_SIZE * CHUNK_SIZE] = val;
}
