#include "win32_adventur.h"

#include <stdio.h>

#include <windows.h>
#include <windowsx.h>

#include <glew.h>
#include <wglew.h>

#include "game_platform.h"
#include "log.h"

static bool globalGameRunning = true;
static double globalTimerResolution;
static Win32Data win32;

static PlatformState platform;

void PlatformSetMouseLocked(bool locked)
{
	platform.input.mouseLocked = locked;
	ShowCursor(!locked);
	if (locked)
	{
		POINT clientCenter = {platform.clientWidth / 2, platform.clientHeight / 2};
		if(ClientToScreen(win32.window, &clientCenter))
		{
			SetCursorPos(clientCenter.x, clientCenter.y);
		}
		platform.input.mouseX = platform.clientWidth / 2;
		platform.input.mouseY = platform.clientHeight / 2;
	}
}

static double Win32ElapsedMS()
{
	LARGE_INTEGER res;
	QueryPerformanceCounter(&res);
	double count = (double)res.QuadPart;
	return count / globalTimerResolution;
}

// TODO: Set input to pass to game
LRESULT CALLBACK WindowProcedure(
	HWND window,
	UINT message,
	WPARAM wParam,
	LPARAM lParam)
{
	LRESULT result = 1;

	switch (message)
	{
		case WM_CLOSE:
		{
			// TODO: Check to see if user actually wants to quit
			DestroyWindow(window);
		}
		break;
		case WM_DESTROY:
		{
			// TODO: Cleanup at this point.
			PostQuitMessage(0);
		}
		break;
		default:
		{
			result = DefWindowProc(window, message, wParam, lParam);
		}
	}

	return result;
}

void Win32HandleButtonMessage(bool currentlyDown, ButtonState *button)
{
	button->endedDown = currentlyDown;
	button->numStateSwitches++;
}

void Win32HandleEvents()
{
	MSG message;
	while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		LPARAM lParam = message.lParam;
		WPARAM wParam = message.wParam;
		switch(message.message)
		{
			case WM_QUIT: globalGameRunning = false; break;
			// Either key up or key down; state change.
			case WM_KEYDOWN:
			case WM_KEYUP:
			{
				bool previouslyDown = ((message.lParam & (1 << 30)) != 0);
				bool currentlyDown = ((message.lParam & (1 << 31)) == 0);
				if (previouslyDown != currentlyDown)
				{
					switch (wParam)
					{
						case 'W':
							Win32HandleButtonMessage(currentlyDown, &platform.input.forwardBtn);
						break;
						case 'S':
							Win32HandleButtonMessage(currentlyDown, &platform.input.backwardBtn);
						break;
						case 'A':
							Win32HandleButtonMessage(currentlyDown, &platform.input.leftBtn);
						break;
						case 'D':
							Win32HandleButtonMessage(currentlyDown, &platform.input.rightBtn);
						break;
						case VK_SPACE:
							Win32HandleButtonMessage(currentlyDown, &platform.input.upBtn);
						break;
						case VK_SHIFT:
							Win32HandleButtonMessage(currentlyDown, &platform.input.downBtn);
						break;
						case VK_ESCAPE: globalGameRunning = false; break;
					}
				}
			} break;
			case WM_MOUSEMOVE:
			{
				platform.input.mouseX = GET_X_LPARAM(lParam);
				platform.input.mouseY = GET_Y_LPARAM(lParam);
			} break;
			case WM_LBUTTONDOWN:
			{
				Win32HandleButtonMessage(true, &platform.input.leftMouseBtn);
			} break;
			case WM_LBUTTONUP:
			{
				Win32HandleButtonMessage(false, &platform.input.leftMouseBtn);
			} break;
			case WM_RBUTTONDOWN:
			{
				Win32HandleButtonMessage(true, &platform.input.rightMouseBtn);
			} break;
			case WM_RBUTTONUP:
			{
				Win32HandleButtonMessage(false, &platform.input.rightMouseBtn);
			} break;
		}
		TranslateMessage(&message);
		DispatchMessageA(&message);
	}
}

void Win32HandlePostUpdate(Win32Data *win32)
{
	// log_info("X:%d, Y:%d", platform.input.mouseX, platform.input.mouseY);
	// Reset input state
	for (int8 i = 0; i < NUM_BUTTONS; ++i)
		platform.input.buttons[i].numStateSwitches = 0;
	if (platform.input.mouseLocked)
	{
		POINT clientCenter = {platform.clientWidth / 2, platform.clientHeight / 2};
		if(ClientToScreen(win32->window, &clientCenter))
		{
			SetCursorPos(clientCenter.x, clientCenter.y);
		}
	}
}

int CALLBACK WinMain(
	HINSTANCE instance,
	HINSTANCE prevInstance,
	LPSTR commandLine,
	int showCode)
{
	log_init();

	win32 = {0};

	platform = {0};
	platform.targetUpdateMs = 1000.L / 120.L;
	platform.targetRenderMs = 1000.L / 60.L;
	platform.clientWidth = 1280;
	platform.clientHeight = 720;

	LARGE_INTEGER queryfreqresult;
	QueryPerformanceFrequency(&queryfreqresult);
	globalTimerResolution = (double)queryfreqresult.QuadPart;
	globalTimerResolution /= 1000.L;

	win32.windowClass.cbSize = sizeof(WNDCLASSEX);
	win32.windowClass.style = CS_OWNDC;
	win32.windowClass.lpfnWndProc = WindowProcedure;
	win32.windowClass.hInstance = instance;
	win32.windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	win32.windowClass.lpszClassName = "AdventurWindowClass";

	if (!RegisterClassEx(&win32.windowClass))
	{
		log_error("Failed to register window class!");
	}

	RECT tempClientRect = {0, 0, platform.clientWidth, platform.clientHeight};
	AdjustWindowRectEx(&tempClientRect, WS_OVERLAPPEDWINDOW, false, 0);

	win32.window = CreateWindowEx(
		0,
		"AdventurWindowClass",
		"Title",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		tempClientRect.right - tempClientRect.left,
		tempClientRect.bottom - tempClientRect.top,
		0,
		0,
		instance,
		0);

	if (!win32.window)
	{
		// TODO: Log
		return 1;
	}


	ShowWindow(win32.window, showCode);
	UpdateWindow(win32.window);

	// Opengl context initialization
	win32.deviceContext = GetDC(win32.window);
	if (!win32.deviceContext)
	{
		// TODO: Log
		printf("Failed to get device context!\n");
		return 1;
	}

	PIXELFORMATDESCRIPTOR descriptor = 
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32, // Color buffer bits
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24, // Depth buffer bits
		8, // Stencil buffer bits
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int pixelFormat = ChoosePixelFormat(win32.deviceContext, &descriptor);
	if (!pixelFormat)
	{
		log_error("Failed to get pixel format descriptor!");
		return 1;
	}

	if (!SetPixelFormat(win32.deviceContext, pixelFormat, &descriptor))
	{
		log_error("Failed to set pixel format!");
		return 1;
	}

	HGLRC tempRenderContext = wglCreateContext(win32.deviceContext);

	wglMakeCurrent(win32.deviceContext, tempRenderContext);

	typedef HGLRC (WINAPI *ContextLoaderFunction)
		(HDC hDC, HGLRC hShareContext, const int *attribList);

	ContextLoaderFunction _wglCreateContextAttribsARB =
		(ContextLoaderFunction) wglGetProcAddress("wglCreateContextAttribsARB");

	if (!_wglCreateContextAttribsARB)
	{
		// TODO: This probably means modern context isn't supported.
		log_error("Failed to get function pointer to load modern context!");
		return 1;
	}

	int gl_attributes[] = 
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 3,
		WGL_CONTEXT_FLAGS_ARB, 0,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0
	};

	win32.glRenderContext = _wglCreateContextAttribsARB(win32.deviceContext, 0, gl_attributes);

	if (!win32.glRenderContext)
	{
		log_error("wglCreateContextAttribsARB failed!");
		return 1;
	}

	wglMakeCurrent(win32.deviceContext, win32.glRenderContext);
	wglDeleteContext(tempRenderContext);

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		log_error("Glew init failed!");
		return 1;
	}

	// TODO: Handle broken renderer
	if (!GLEW_VERSION_3_3)
	{
		log_error("This computer doesn't support OpenGL 3.3+!");
		return 1;
	}

	double current_time = 0;
	double elapsed_time = 0;
	double update_elapsed = 0;
	double render_elapsed = 0;

	GameInit();

	double prev_time = Win32ElapsedMS();

	// Enter main loop
	while (globalGameRunning)
	{	
		Win32HandleEvents();
		current_time = Win32ElapsedMS();
		elapsed_time = current_time - prev_time;
		platform.elapsedTime += elapsed_time;

		if (elapsed_time > 100)
		{
			// TODO: Log and diagnose delay
			elapsed_time = 100;
		}

		prev_time = current_time;

		update_elapsed += elapsed_time;
		render_elapsed += elapsed_time;

		// Update counter
		while (update_elapsed >= platform.targetUpdateMs)
		{
			GameUpdate(&platform);
			Win32HandlePostUpdate(&win32);
			update_elapsed -= platform.targetUpdateMs;
		}

		if (render_elapsed >= platform.targetRenderMs)
		{
			// render
			GameRender(update_elapsed / platform.targetUpdateMs);
			SwapBuffers(win32.deviceContext);
			render_elapsed = 0;
		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	return WinMain(GetModuleHandle(NULL), NULL, GetCommandLine(), SW_SHOW);
}

#include "game.cpp"
